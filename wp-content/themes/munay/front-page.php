<?php /* Template Name: Inicio */ ?>
<?php get_header(); ?>
<?php echo do_shortcode('[vsz_slick_slider]');?>

<div class="nosotros-inicio estrecho">
  <h3>Sobre Munay Minerales</h3>
  <div class="datos">
    <div class="img">
      <img src="/wp-content/uploads/2019/10/sobre-1.jpg" alt="">
    </div>
    <div class="info-nosotros">
      <p>Os damos la bienvenida a la web del Reino Mineral.</p>
      <br>
      <p>En Munay Minerales podrás encontrar información acerca de las propiedades
      sanadoras de los minerales. Através de nuestros talleres te enseñaremos a limpiar,
      recarga, programar, meditar y alinear tus chakras con diferentes minerales, asi como
      aprender acerca de sus beneficiosas propiedades. Cómo usarlos en tu vida cotidiana
      y conectar con ellos de maneras diferentes.
      </p>
      <br>
      <p>Disponemos de una amplia variedad de minerales de sanación así como para coleccionismo.</p>
      <br>
      <p>Desde Munay Minerales, abrimos un espacio de integración y crecimiento personal.
      Tomaremos un tiempo para reconectar con la sabiduría de la selva amazónica a través de sus plantas.</p>
      <br>
      <p>Puedes visitarnos en calle Josep d'Oleza junto al número 2, local 16 (cerca del Parc de ses Fonts) Palma de Mallorca, Islas Baleares. Atendemos con cita previa.</p>
    </div>
  </div>
  <img src="/wp-content/uploads/2019/10/crystal-1.png" alt="" class="cristal-1">
  <img src="/wp-content/uploads/2019/10/crystal-2.png" alt="" class="cristal-2">
</div>

<?php echo do_shortcode('[nuestros_productos]');?>
<?php get_footer(); ?>
