<div class="toggle-btn">
    <span class="abrir"><img src="/wp-content/uploads/2020/05/filtro.png" alt="icono filtrado"></span>
</div>
<aside id="sidebar">
    <?php if ( is_active_sidebar( 'primary-widget-area' ) ) : ?>
    <div id="primary" class="widget-area">
        <h2 class="filtro">Filtrar por:</h2>
        <ul class="xoxo">
            <?php dynamic_sidebar( 'primary-widget-area' ); ?>
            <!--  -->
        </ul>
    </div>
    <?php endif; ?>
</aside>