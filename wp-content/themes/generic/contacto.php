<?php /* Template Name: Contacto */ ?>
<?php get_header(); ?>
<?php echo do_shortcode('[vsz_slick_slider]');?>
<div class="container-contacto estrecho">
  <div class="page-info">
    <h3>Contáctanos</h3>
    <div class="datos-page">
      <h4>Email:</h4>
      <p>rivasfaib@gmail.com</p>
      <h4>Teléfono:</h4>
      <p>+34 633213187</p>
    <p>+34 655110727 (Whatsapp)</p>
    <p>+51 987 559 180 (Whatsapp)</p>
      <h4>Dirección:</h4>
      <p>Calle Josep d'Oleza n.2, local 16,</p>
      <p>Palma de Mallorca, España</p>
    </div>

  </div>
  <div class="form-contacto">
    <h3>Déjanos tu mensaje</h3>
    <?php echo do_shortcode('[contact-form-7 id="155" title="Contacto"]');?>
  </div>
</div>
<div class="mapa">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d768.6668788359207!2d2.65022307810927!3d39.58964313433062!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x129792fac74c8061%3A0x440a0f36df93cb15!2sCarrer%20de%20Josep%20d&#39;Oleza%2C%2007010%20Palma%2C%20Illes%20Balears%2C%20Espa%C3%B1a!5e0!3m2!1ses!2spe!4v1572901897112!5m2!1ses!2spe" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<?php get_footer(); ?>
