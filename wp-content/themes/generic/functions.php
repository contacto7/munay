<?php
add_action( 'after_setup_theme', 'generic_setup' );
function generic_setup() {
load_theme_textdomain( 'generic', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'search-form' ) );
global $content_width;
if ( ! isset( $content_width ) ) { $content_width = 1920; }
register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'generic' ) ) );
}
add_action( 'wp_enqueue_scripts', 'generic_load_scripts' );
function generic_load_scripts() {
wp_enqueue_style( 'generic-style', get_stylesheet_uri() );
wp_enqueue_script( 'jquery' );
wp_register_script( 'generic-videos', get_template_directory_uri() . '/js/videos.js' );
wp_enqueue_script( 'generic-videos' );
wp_add_inline_script( 'generic-videos', 'jQuery(document).ready(function($){$("#wrapper").vids();});' );
}
add_action( 'wp_footer', 'generic_footer_scripts' );
function generic_footer_scripts() {
?>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
$(":checkbox").on("click", function () {
$(this).parent().toggleClass("checked");
});
});
</script>
<?php
}
add_filter( 'document_title_separator', 'generic_document_title_separator' );
function generic_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
add_filter( 'the_title', 'generic_title' );
function generic_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'the_content_more_link', 'generic_read_more_link' );
function generic_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'generic_excerpt_read_more_link' );
function generic_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'intermediate_image_sizes_advanced', 'generic_image_insert_override' );
function generic_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
return $sizes;
}
add_action( 'widgets_init', 'generic_widgets_init' );
function generic_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'generic' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
add_action( 'wp_head', 'generic_pingback_header' );
function generic_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
add_action( 'comment_form_before', 'generic_enqueue_comment_reply_script' );
function generic_enqueue_comment_reply_script() {
if ( get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
}
function generic_custom_pings( $comment ) {
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'generic_comment_count', 0 );
function generic_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

function my_login_logo_url_title() {
   return 'Ir a la página';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
function my_login_logo_url() {
   return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo() { ?>
   <style type="text/css">
       #login h1 a, .login h1 a {
           background-image: url(../wp-content/uploads/2019/10/logo-header.png);
           height: 114px;
           width: 207px;
           background-size: 100%;
           background-repeat: no-repeat;
       }
   </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
// crear taxonomias en productos


  function nuestros_productos(){
    ?>
  	<section class="nuestros-productos  estrecho">
  	<h2 class="section-title"> Nuestros Productos</h2>
    <div id="search">
      <form role="search" method="get" class="woocommerce-product-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
      	<label class="screen-reader-text" for="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>"><?php esc_html_e( 'Search for:', 'woocommerce' ); ?></label>
      	<input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field hola" placeholder="Buscar ..." value="<?php echo get_search_query(); ?>" name="s" />
      	<button type="submit" class="buscar-page" value="<?php echo esc_attr_x( 'Search', 'submit button', 'woocommerce' ); ?>"><img src="./wp-content/uploads/2019/10/icon-search.png" alt=""></button>
      	<input type="hidden" name="post_type" value="product" />
      </form>
    </div>
  	   <?php
  	echo do_shortcode('[featured_products orderby="date" order="DESC"]');

  ?>
  </section> <?php
  }
  	add_shortcode('nuestros_productos', 'nuestros_productos');



/* se agrego <?php echo do_shortcode('[atributes]');?> en WooCommerce/templates/loop/price.php para que muestre los atributos del producto;*/
    //  function atributes() {
    // 	//template for this is in storefront-child/woocommerce/single-product/product-attributes.php
    // 	global $product;
    // 	echo $product->list_attributes();
    // }
    // add_shortcode('atributes', 'atributes');


    /**
 * Mostrar 3 productos relacionados
 */
function woo_related_products_limit() {
  global $product;

	$args['posts_per_page'] = 3;
	return $args;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	return $args;
}

    //
    // add_filter( 'woocommerce_breadcrumb_defaults', 'cambiar_separador_breadcrumbs' );
    // function cambiar_separador_breadcrumbs( $defaults ) {
    // 	$defaults['delimiter'] = ' » ';
    // 	return $defaults;
    // }

//Te muestra los nombres de las categorias
    // function productos_page() {
    //   global $post;
    //     $html = "";
    //     $my_query = new WP_Query( array(
    //       'post_type' => 'product',
    //       'posts_per_page' => -1
    //     ));
    //     $categories = get_categories( array(
    //    'orderby' => 'name',
    //    'parent'  => 0,
    //    'taxonomy' => 'product_cat',
    // ) );
    // $args = array(
    //    'orderby' => 'name',
    //    'taxonomy'=>'product_cat',
    //    'parent' => 0
    // );
    //
    // foreach ( $categories as $product_cat ) {
    //    echo '<li><a data-toggle="tab" href="#'.$product_cat->name.'">' . $product_cat->name . '</a></li>';
    // }
    //
    // }
    // add_shortcode('productos_page', 'productos_page');
    //


add_filter('woocommerce_catalog_orderby', 'munay_cambiar_sort', 40);
function munay_cambiar_sort($filtro) {
  $filtro['price-desc'] =__('Precio de mayor a menor', 'woocommerce');
  $filtro['price'] = __('Precio de menor a mayor', 'woocommerce');
  $filtro['date'] = __('Ultimos productos', 'woocommerce');
  $filtro['rating'] = __('Productos con Calificación', 'woocommerce');
  $filtro['popularity'] = __('Productos más populares', 'woocommerce');
  $filtro['menu_order'] = __('Por defecto', 'woocommerce');
  return $filtro;
}



// function munay_no_imagen_destacada($imagen_url) {
//   $imagen_url = 'http://18.219.190.143/wp-content/uploads/2019/10/logo-header.png';
//   return $imagen_url;
// }
// add_filter('woocommerce_placeholder_img_src', 'munay_no_imagen_destacada');
 ?>
