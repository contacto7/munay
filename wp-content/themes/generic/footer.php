</div>
<footer id="footer">
  <div class="logo-footer">
    <a href="http://18.219.190.143/">
      <img src="/wp-content/uploads/2019/10/logo-footer.png" alt="">
    </a>
  </div>
  <div class="contacto-footer">
    <h3>Contacto</h3>
    <p>+34 633213187</p>
    <p>+34 655110727 (Whatsapp)</p>
    <p>+51 987 559 180 (Whatsapp)</p>
    <p>rivasfaib@gmail.com</p>
    <p>Calle Josep d'Oleza n.2, local 16</p>
    <p>Palma de Mallorca, España</p>
  </div>
  <div class="servicio-footer">
    <h3>Servicio al cliente</h3>
    <p><a href="/envios-y-devoluciones">Envíos y devoluciones</a></p>
    <p><a href="/aviso-legal/">Aviso Legal</a></p>
    <p><a href="/terminos-condiciones">Términos y condiciones</a></p>
    <p><a href="/politica-privacidad">Política de Privacidad</a></p>
    <p><a href="/politica-cookies/">Política de Cookies</a></p>
  </div>
  <img src="/wp-content/uploads/2019/10/icon-inoloop-footer.png" alt="" class="logo-inoloop">
</footer>
<div id="copyright">
&copy; <?php echo esc_html( date_i18n( __( 'Y', 'generic' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
</div>
</div>
<?php wp_footer(); ?>
<script>
  // secondary toggle
const btnToggle = document.querySelector('.toggle-btn');

btnToggle.addEventListener('click', function () {
  console.log('clik')
  document.getElementById('primary').classList.toggle('active');
  console.log(document.getElementById('primary'))
});

</script>
</body>
</html>
