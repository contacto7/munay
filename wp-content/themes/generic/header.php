<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header">
<div id="branding" class="estrecho">
  <div class="redes-header">
    <a href="https://www.facebook.com/munayminerales/"><img src="/wp-content/uploads/2019/11/icon-fb.png" alt=""></a>
    <a href="https://www.instagram.com/munayminerales/"><img src="/wp-content/uploads/2019/11/icon-insta.png" alt=""></a>
    <a href="https://api.whatsapp.com/send?phone=34655110727&text=Hola%20Estoy%20interesado%20en%20un%20producto!"><img src="/wp-content/uploads/2019/11/icon-wsp.png" alt=""></a>
    <a href="https://www.youtube.com/channel/UCZcp2B7tgKLg0vKp7Dmgb3Q"><img src="/wp-content/uploads/2019/11/icon-youtube-1.png" alt=""></a>
  </div>
  <div class="logo-header">
    <a href="http://18.219.190.143/"><img src="/wp-content/uploads/2019/10/logo-header.png" alt=""></a>
  </div>
  <div class="info-header">
    <div class="item"><p><a href="mailto:rivasfaib@gmail.com">rivasfaib@gmail.com</a></p><img src="/wp-content/uploads/2019/10/crystal-icon-headar.png" alt=""></div>
    <div class="item"><p>+34 633213187 y +34 655110727</p><img src="/wp-content/uploads/2019/10/crystal-icon-headar.png" alt=""></div>
    <div class="item"><p>Calle Josep d'Oleza n.2, local 16, Palma de Mallorca, España</p><img src="/wp-content/uploads/2019/10/crystal-icon-headar.png" alt=""></div>
  </div>
</div>
<nav id="menu">
<label class="toggle" for="toggle"><span class="menu-icon">&#9776;</span> <span class="menu-text">Menú</span></label>
<input id="toggle" class="toggle" type="checkbox" />
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
</header>
<div id="container">
