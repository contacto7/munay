<?php
/*
Plugin Name: Metaboxes CMB2
Plugin URI:
Description: Agrega MetaBoxes
Version: 1.0
Author URI:
License: GLP2
Licence URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
if ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

// add_action( 'cmb2_admin_init', 'campos_banner' );
//
// function campos_banner() {
// 	$prefix = 'ga_campos_banner_';
//
// $metabox_banner = new_cmb2_box( array(
// 		'id'            => $prefix . 'metabox',
// 		'title'         => __( 'Campos Banner', 'cmb2' ),
// 		'object_types' => array( 'page' ), // Post type
// 	) );
//   $metabox_banner->add_field( array(
//     'name' => __( 'Imagen', 'cmb2' ),
//     'id' => $prefix . 'imagen',
//     'type' => 'file',
//   ));
//
// }


// function yourprefix_register_main_options_metabox() {
// 	/**
// 	 * Registers main options page menu item and form.
// 	 */
// 	$args = array(
// 		'id'           => 'yourprefix_main_options_page',
// 		'title'        => 'Preferencias de Usuario',
// 		'object_types' => array( 'options-page' ),
// 		'option_key'   => 'yourprefix_main_options',
// 	);
// 	// 'tab_group' property is supported in > 2.4.0.
// 	if ( version_compare( CMB2_VERSION, '2.4.0' ) ) {
// 		$args['display_cb'] = 'yourprefix_options_display_with_tabs';
// 	}
// 	$main_options = new_cmb2_box( $args );
// 	/**
// 	 * Options fields ids only need
// 	 * to be unique within this box.
// 	 * Prefix is not needed.
// 	 */
//
//
//
//   $main_options->add_field( array(
//     'name' => __( 'Titulo', 'cmb2' ),
//     'id' =>'title1',
//     'type' => 'text',
//   ));
//   $main_options->add_field( array(
//     'name' => __( 'Descripcion', 'cmb2' ),
//     'id' =>'subtitle1',
//     'type' => 'text',
//   ));
//   $main_options->add_field( array(
//     'name' => __( 'Imagen', 'cmb2' ),
//     'id' =>'imagen1',
//     'type' => 'file',
//   ));
//
// }
// add_action( 'cmb2_admin_init', 'yourprefix_register_main_options_metabox' );
